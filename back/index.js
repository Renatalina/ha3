const express = require("express");
const MongoClient = require("mongodb").MongoClient;
const ObjectId = require("mongodb").ObjectId;
const jwt = require("jsonwebtoken");

const app = express();
const jsonParser = express.json();

const uri =
  "mongodb+srv://renatalina:htv,02602@cluster0.jrpld.mongodb.net/test";

const mongoClient = new MongoClient(uri);

const tokenKey = "1a2b-3c4d-5e6f-7g8h";
let dbClient;
const meUser = {};

app.use(express.static(__dirname + "/public"));

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, PUT, PATCH, POST, DELETE");
  res.header("Access-Control-Allow-Headers", "Content-Type");
  if (req.headers.authorization) {
    jwt.verify(
      req.headers.authorization.split(" ")[1],
      tokenKey,
      (err, payload) => {
        if (err) next();
        else if (payload) {
          for (const user of users) {
            if (user.password === payload.password) {
              req.user = user;
              next();
            }
          }

          if (!req.user) next();
        }
      }
    );
  }

  next();
});

mongoClient.connect(function (err, client) {
  if (err) return console.log(err);
  dbClient = client;
  app.locals.collection = client.db("uber").collection("users");
  app.locals.notes = client.db("uber").collection("notes");
  app.locals.trucks = client.db("uber").collection("trucks");
  app.locals.loads = client.db("uber").collection("loads");
  app.listen(8080, function () {
    console.log("Сервер ожидает подключения на 8080");
  });
});

app.post("/api/auth/register", jsonParser, function (req, res) {
  try {
    const userEmail = req.body.email;
    const userPassword = req.body.password;
    const userRole = req.body.role;

    const user = { email: userEmail, password: userPassword, role: userRole };

    const collection = req.app.locals.collection;
    collection.insertOne(user, function (err, result) {
      if (err) return res.status(400).send({ message: "string" });

      meUser._id = result.insertedId.toHexString();
      meUser.email = userEmail;
      meUser.password = userPassword;
      meUser.role = userRole;
      jwt_token = "0";
      res.status(200).send({ message: "Profile created successfully" });
    });
  } catch (error) {
    res.status(500).send({ message: "string" });
  }
});

app.post("/api/auth/login", jsonParser, function (req, res) {
  try {
    if (!req.body) return res.status(400).send({ message: "string" });

    const userEmail = req.body.email;
    const userPassword = req.body.password;

    const user = { email: userEmail, password: userPassword };

    const collection = req.app.locals.collection;
    collection.findOne(user, function (err, result) {
      if (err) return res.status(400).send({ message: "string" });

      if (result) {
        meUser._id = result._id.toHexString();
        meUser.email = result.email;
        meUser.password = result.password;
        meUser.role = result.role;

        res.status(200).send({
          message: "Success",
          jwt_token: `${jwt.sign({ password: userPassword }, tokenKey)}`,
        });
      }
    });
  } catch (error) {
    res.status(500).send({ message: "string" });
  }
});

app.post("/api/auth/forgot_password", jsonParser, function (req, res) {
  try {
    if (!req.body) return res.status(400).send({ message: "string" });

    const userEmail = req.body.email;

    const user = { email: userEmail };

    const collection = req.app.locals.collection;
    collection.findOne(user, function (err, result) {
      if (err) return res.status(400).send({ message: "string" });

      if (result) {
        meUser._id = result._id.toHexString();
        meUser.email = userEmail;
        meUser.password = createRandomPassword();

        collection.findOneAndUpdate(
          { _id: meUser._id },
          {
            $set: {
              password: meUser.password,
            },
          },
          { returnDocument: "after" },
          function (err, result) {
            if (err) return res.status(500).send({ message: "string" });
            res.status(200).send({
              message: "New password sent to your email address",
            });
          }
        );
      }
    });
  } catch (error) {
    res.status(500).send({ message: "string" });
  }
});

function createRandomPassword() {
  let text = "";
  let possible =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (let i = 0; i < 6; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}

app.get("/api/users/me", function (req, res) {
  try {
    const collection = req.app.locals.collection;

    collection.findOne({ _id: new ObjectId(meUser._id) }, function (err, user) {
      if (err) {
        res.status(400).send({ message: "string" });
        return console.log(err);
      }
      res.status(200).send({
        user: {
          _id: meUser._id,
          role: meUser.role,
          email: meUser.email,
          createDate: new Date().toLocaleDateString(),
        },
      });
    });
  } catch (error) {
    res.status(500).send({ message: "sting" });
  }
});

app.delete("/api/users/me", function (req, res) {
  try {
    const collection = req.app.locals.collection;
    collection.findOneAndDelete(
      { _id: new ObjectId(meUser._id) },
      function (err, result) {
        if (err) {
          res.status(400).send({ message: "string" });
          return console.log(err);
        }
        res.status(200).send({ message: "Success" });
      }
    );
  } catch (error) {
    res.status(500).send({ message: "string" });
  }
});

app.patch("/api/users/me/password", jsonParser, function (req, res) {
  try {
    const id = new ObjectId(meUser._id);
    const userPassword = req.body.oldPassword;
    const newPassword = req.body.newPassword;

    const collection = req.app.locals.collection;

    collection.findOneAndUpdate(
      { _id: id, password:userPassword },
      { $set: { password: newPassword } },
      { returnDocument: "after" },
      function (err, result) {
        if (err) return res.status(400).send({ message: "string" });

        res.status(200).send({ message: "Password changed successfully" });
      }
    );
  } catch (error) {
    res.status(500).send({ message: "string" });
  }
});

app.post("/api/trucks", jsonParser, function (req, res) {
  try {
    if (!req.body)
      return res.status(400).send({ message: "Your request is empty" });
      console.log(meUser.role==="DRIVER");

    if (!meUser.role === "DRIVER")
      return res
        .status(400)
        .send({ message: "You haven't role to add trucks" });

    const idMg = new ObjectId().toHexString();

    const truckType = req.body.type;
    const truck = {
      _id: idMg,
      created_by: meUser._id,
      assigned_to: idMg,
      type: truckType,
      status: "IS",
      created_date: new Date().toLocaleString(),
    };

    const collection = req.app.locals.trucks;
    collection.insertOne(truck, function (err, result) {
      if (err) return res.status(400).send({ message: "string" });

      if (result)
        res.status(200).send({
          message: "Truck created successfully",
        });
    });
  } catch (error) {
    res.status(500).send({ message: "string" });
  }
});

app.get("/api/trucks", function (req, res) {
  try {
    if (!meUser.role === "DRIVER")
      return res
        .status(400)
        .send({ message: "You haven't role to add trucks" });

    const collection = req.app.locals.trucks;
    collection.find().toArray(function (err, trucks) {
      if (err)
        return res.status(400).send({ message: "None of trucks is find" });
      res.status(200).send({
        trucks: trucks,
      });
    });
  } catch (error) {
    res.status(500).send({ message: "Server is error" });
  }
});

app.get("/api/trucks/:id", function (req, res) {
  try {
    if (!meUser.role === "DRIVER")
      return res
        .status(400)
        .send({ message: "You haven't role to add trucks" });

    const collection = req.app.locals.trucks;
    collection.findOne({ _id: req.params.id }, function (err, truck) {
      if (err) return res.status(400).send({ message: "string" });
      res.status(200).send({
        truck: truck,
      });
    });
  } catch (error) {
    res.status(500).send({ message: "Server is error" });
  }
});

app.put("/api/trucks/:id", jsonParser, function (req, res) {
  try {
    if (!meUser.role === "DRIVER")
      return res
        .status(400)
        .send({ message: "You haven't role to add trucks" });

    const truckType = req.body.type;

    const collection = req.app.locals.trucks;
    collection.findOneAndUpdate(
      { _id: req.params.id },
      {
        $set: {
          type: truckType,
        },
      },
      { returnDocument: "after" },
      function (err, result) {
        if (err) return res.status(400).send({ message: "Change didn't save" });

        res.status(200).send({ message: "Truck details changed successfully" });
      }
    );
  } catch (error) {
    res.status(500).send({ message: "Server is error" });
  }
});

app.delete("/api/trucks/:id", function (req, res) {
  try {
    if (!meUser.role === "DRIVER")
      return res
        .status(400)
        .send({ message: "You haven't role to add trucks" });

    const collection = req.app.locals.trucks;
    collection.findOneAndDelete({ _id: req.params.id }, function (err, result) {
      if (err) {
        return res.status(400).send({ message: "Trucks can't delite" });
      }
      res.status(200).send({ message: "Truck deleted successfully" });
    });
  } catch (error) {
    res.status(500).send({ message: "Server is error" });
  }
});

app.post("/api/trucks/:id/assign", jsonParser, function (req, res) {
  try {
    if (!req.body)
      return res.status(400).send({ message: "Your request is empty" });
    if (!meUser.role === "DRIVER")
      return res
        .status(400)
        .send({ message: "You haven't role to add trucks" });

    const assignedLoad = req.body.assigned;
    const collection = req.app.locals.trucks;
    const collectionAssigned = req.app.locals.loads;

    collectionAssigned.findOneAndUpdate(
      { _id: assignedLoad },
      {
        $set: {
          assigned_to: req.params.id,
          status: "ASSIGNED",
          logs: [
            {
              message: `Load assigned to driver with id ${meUser._id}`,
              time: new Date().toLocaleString(),
            },
          ],
        },
      },
      { returnDocument: "after" },
      function (err, result) {
        if (err)
          return res.status(400).send({ message: "Assigned can't save" });

        collection.findOneAndUpdate(
          { _id: req.params.id },
          {
            $set: {
              assigned_to: assignedLoad,
            },
          },
          { returnDocument: "after" },
          function (err, result) {
            if (err)
              return res.status(400).send({ message: "Assigned can't save" });

            res.status(200).send({ message: "Truck assigned successfully" });
          }
        );
      }
    );
  } catch (error) {
    res.status(500).send({ message: "string" });
  }
});

app.post("/api/loads", jsonParser, function (req, res) {
  try {
    if (!req.body)
      return res.status(400).send({ message: "Your request is empty" });
    if (!meUser.role === "SHIPPER")
      return res.status(400).send({ message: "You haven't role to add loads" });

    const idMg = new ObjectId().toHexString();

    const load = {
      _id: idMg,
      created_by: meUser._id,
      assigned_to: idMg,
      status: "NEW",
      state: "En route to Pick Up",
      name: req.body.name,
      payload: req.body.payload,
      pickup_address: req.body.pickup_address,
      delivery_address: req.body.delivery_address,
      dimensions: {
        width: req.body.dimensions.width,
        length: req.body.dimensions.length,
        height: req.body.dimensions.height,
      },
      logs: [],
      created_date: new Date().toLocaleString(),
    };

    const collection = req.app.locals.loads;
    collection.insertOne(load, function (err, result) {
      if (err) return res.status(400).send({ message: "Loads can't creat" });

      if (result)
        res.status(200).send({
          message: "Load created successfully",
        });
    });
  } catch (error) {
    res.status(500).send({ message: "string" });
  }
});

app.get("/api/loads", function (req, res) {
  try {
    const collection = req.app.locals.loads;
    const offsetPagination = req.query.offset;

    if (meUser.role === "SHIPPER")
      collection
        .find({ created_by: meUser._id, status: req.query.status })
        .limit(req.query.limit)
        .toArray(function (err, loads) {
          if (err) return res.status(400).send({ message: "string" });
          res.status(200).send({
            offset: offsetPagination,
            loads: [loads],
          });
        });

    if (meUser.role === "DRIVER")
      // collection.find({assigned_to: meUser._id, status: req.query.status, logs: {message:{$regex: `${meUser._id}$`} }}).limit(req.query.limit).toArray(function (err, loads) {
      collection
        .find({ status: req.query.status })
        .limit(req.query.limit)
        .toArray(function (err, loads) {
          if (err) return res.status(400).send({ message: "string" });
          res.status(200).send({
            offset: offsetPagination,
            loads: [loads],
          });
        });
  } catch (error) {
    res.status(500).send({ message: "string" });
  }
});

app.get("/api/loads/active", function (req, res) {
  try {
    if (!meUser.role === "DRIVER")
      return res.status(400).send({ message: "You haven't role to add loads" });

    const collection = req.app.locals.loads;
    collection
      .find({
        status: "ASSIGNED",
        logs: { message: { $regex: `${meUser._id}$` } },
      })
      .toArray(function (err, loads) {
        if (err) return res.status(400).send({ message: "string" });
        res.status(200).send({
          loads: [loads],
        });
      });
  } catch (error) {
    res.status(500).send({ message: "string" });
  }
});

app.patch("/api/loads/active/state", function (req, res) {
  try {
    if (!meUser.role === "DRIVER")
      return res
        .status(400)
        .send({ message: "You haven't role to patch loads" });

    const collection = req.app.locals.loads;

    collection.findOneAndUpdate(
      { _id: req.query.id },
      {
        $set: {
          state: "En route to Delivery",
        },
      },
      { returnDocument: "after" },
      function (err, loads) {
        if (err) return res.status(400).send({ message: "string" });
        res
          .status(200)
          .send({ message: "Load state changed to 'En route to Delivery'" });
      }
    );
  } catch (error) {
    res.status(500).send({ message: "string" });
  }
});

app.get("/api/loads/:id", function (req, res) {
  try {
    const collection = req.app.locals.loads;
    collection.findOne({ _id: req.params.id }, function (err, load) {
      if (err)
        return res.status(400).send({ message: "None of truck is find" });
      res.status(200).send({
        load: load,
      });
    });
  } catch (error) {
    res.status(500).send({ message: "Server is error" });
  }
});

app.put("/api/loads/:id", jsonParser, function (req, res) {
  try {
    if (!meUser.role === "SHIPPER")
      return res
        .status(400)
        .send({ message: "You haven't role to add trucks" });

    // may be set a some check?
    const collection = req.app.locals.loads;
    collection.findOneAndUpdate(
      { _id: req.params.id },
      {
        $set: {
          name: req.body.name,
          payload: req.body.payload,
          pickup_address: req.body.pickup_address,
          delivery_address: req.body.delivery_address,
          dimensions: {
            width: req.body.dimensions.width,
            length: req.body.dimensions.length,
            height: req.body.dimensions.height,
          },
        },
      },
      { returnDocument: "after" },
      function (err, result) {
        if (err) return res.status(400).send({ message: "Change didn't save" });

        res.status(200).send({ message: "Loads details changed successfully" });
      }
    );
  } catch (error) {
    res.status(500).send({ message: "Server is error" });
  }
});

app.delete("/api/loads/:id", function (req, res) {
  try {
    if (!meUser.role === "SHIPPER")
      return res
        .status(400)
        .send({ message: "You haven't role to delete loads" });

    const collection = req.app.locals.loads;
    collection.findOneAndDelete({ _id: req.params.id }, function (err, result) {
      if (err) {
        return res.status(400).send({ message: "Loads can't delite" });
      }
      res.status(200).send({ message: "Loads deleted successfully" });
    });
  } catch (error) {
    res.status(500).send({ message: "Server is error" });
  }
});

app.post("/api/loads/:id/post", function (req, res) {
  try {
    if (!meUser.role === "SHIPPER")
      return res
        .status(400)
        .send({ message: "You haven't role to find trucks" });

    const collection = req.app.locals.loads;
    collection.findOneAndUpdate(
      { _id: req.params.id },
      {
        $set: {
          status: "POSTED",
        },
      },
      { returnDocument: "after" },
      function (err, result) {
        if (err) return res.status(400).send({ message: "Change didn't save" });

        res.status(200).send({
          message: "Load posted successfully",
          driver_found: true,
        });
      }
    );
  } catch (error) {
    res.status(500).send({ message: "Server is error" });
  }
});

app.post("/api/loads/:id/shipping_info", jsonParser, function (req, res) {
  try {
    if (!req.body)
      return res.status(400).send({ message: "Your request is empty" });
    if (!meUser.role === "SHIPPER")
      return res
        .status(400)
        .send({ message: "You haven't role to get info of loads" });

    const collection = req.app.locals.trucks;
    const collectionLoads = req.app.locals.loads;

    collectionLoads.findOne({ _id: req.params.id }, function (err, load) {
      if (err)
        return res.status(400).send({ message: "None of loads is find" });

      collection.findOne({ _id: load.assigned_to }, function (err, truck) {
        if (err)
          return res.status(400).send({
            load: load,
            truck: "None of truck is find",
          });

        res.status(200).send({
          load: load,
          truck: truck,
        });
      });
    });
  } catch (error) {
    res.status(500).send({ message: "Server is error" });
  }
});

process.on("SIGINT", () => {
  dbClient.close();
  process.exit();
});

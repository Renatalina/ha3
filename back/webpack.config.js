const path = require("path");

module.exports = {
  mode: "none",
  entry: "./public/app.js",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "bundle.js",
  },
  resolve: {},
  module: {
    rules: [],
  },
  plugins: [],
  devtool: "source-map",
  devServer: {
    contentBase: "./dist",
    port: 3000,
    historyApiFallback: {
      index: "index.html",
    },
  },
};

import { LoginController } from "./pages/login";
import { MainPages } from "./pages/main/main-controller";
import { ProfileController } from "./pages/profile/profile-controller";

export default class Router {
  constructor(appBuffer) {
    this.appBuffer = appBuffer;
    this.routes = {}; // page classes for routes
    this.pages = {}; // already created pages (instances)
    this.setListeners();

    this.init();
  }

  init() {
    this.addDefaultRoute("login", LoginController);
    // this.addRoute("posted-loads", PostedLoads);
    // this.addRoute("new-loads", AddLoads);
    // this.addRoute("assigned-loads", AssignedLoads);
    // this.addRoute("history", HistoryLoads);
    this.addRoute("profile", ProfileController);
    this.addRoute("login", LoginController);
    this.addRoute("main-page", MainPages);


    this.navigate(window.location.pathname);
  }

  navigate(href, data, isPopstateEvent) {
    const newUrl = window.location.origin + "/";
    const clearHref = href.replace(/^\//, "");

    let route = this.checkRouteExists(clearHref) || this.defaultRoute;

    if (!this.pages[route]) {
      const taskPage = new this.routes[route](data);
      this.pages[route] = taskPage.init(data);
    }

    let curHref = this.checkRouteExists(clearHref) ? clearHref : "";

    const method = isPopstateEvent ? "replaceState" : "pushState";
    window.history[method](newUrl + curHref, newUrl, newUrl + curHref);
  }

  checkRouteExists(route) {
    if (!route) return false;
    return this.routes[route] && route;
  }

  setListeners() {
    window.addEventListener("click", (event) => {
      let target = event.target.closest("a");
      if (target) {
        if (target.dataset.routerIgnore) return;

        event.preventDefault();
        this.navigate(
          target.getAttribute("href"),
          target.getAttribute("data-set")
        );

        return;
      }

      target = event.target.closest("button");
      if (target && target.dataset.route) {
        event.preventDefault();
        this.navigate(target.dataset.route, target.getAttribute("data-set"));
      }
    });

    window.addEventListener("popstate", (event) => {
      event.preventDefault();
      this.navigate(window.location.pathname, null, true);
    });
  }

  addRoute(name, route) {
    this.routes[name] = route;
  }

  addDefaultRoute(route) {
    this.defaultRoute = route;
  }

  removeRoute(name) {
    delete this.routes[name];
  }
}

export class LoginModel {
  constructor() {
    this.init();
  }

  init() {
    console.log("login model");
  }

  async getUser(user) {
  const responseUser= await fetch("http://localhost:8080/api/auth/login", {
      method: "POST",
      mode: "cors",
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: user.email,
        password: user.password,
      }),
    })

    if (responseUser.ok === true) {
      let user = await responseUser.json();
      return user;
    }
    
    return responseUser.statusText;
  }

  async registrUser(user) {
    const responseUser= await fetch("http://localhost:8080/api/auth/register", {
        method: "POST",
        mode: "cors",
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: user.email,
          password: user.password,
          role: user.role
        }),
      })
  
      if (responseUser.ok === true) {
        let user = await responseUser.json();
        return user;
      }
      
      return responseUser.statusText;
    }

    async forgotPasswordUser(user) {
      const responseUser= await fetch("http://localhost:8080/api/auth/forgot_password", {
          method: "POST",
          mode: "cors",
          headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            email: user.email, 
          }),
        })
    
        if (responseUser.ok === true) {
          let user = await responseUser.json();
          return user;
        }
        
        return responseUser.statusText;
      }
  


}

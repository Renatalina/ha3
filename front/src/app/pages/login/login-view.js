import formLogin from "./formLogin.hbs";

export class LoginView {
  constructor() {
    this.init();
  }

  init() {
    this.main = document.getElementsByTagName("section")[0];
    this.main.innerHTML = formLogin();
  }

  getFormEmailPassword(){

    const form = document.forms["userForm"];  
    const email = form.elements["email"].value;
    const password = form.elements["password"].value;

    return {email, password};
  }

  registrationUser(){

    const form = document.forms["userForm"];  
    const email = form.elements["email"].value;
    const password = form.elements["password"].value;
    const indexRole=form.elements["select-role"].selectedIndex;
    let role;
    if(indexRole===1){
        role="SHIPPER";
    }else if(indexRole===2){
        role="DRIVER";
    }

    return {email, password, role};
  }

  forgotPassword(){
      
    const form = document.forms["userForm"];  
    const email = form.elements["email"].value;

    return {email};
      
  }

}

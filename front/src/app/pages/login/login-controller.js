import { LoginModel } from "./login-model";
import { LoginView } from "./login-view";

export class LoginController {
  constructor() {
    this.loginView = new LoginView();
    this.loginModel = new LoginModel();
  }

  init() {
    this.btnLogin = document.getElementsByClassName("btn-login")[0];
    this.btnLogin.addEventListener("click", this.submitLogin.bind(this));
    this.btnForgotPassword = document.getElementsByClassName("btn-forgot")[0];
    this.btnForgotPassword.addEventListener(
      "click",
      this.submitForgotPassword.bind(this)
    );
    this.btnRegistration =
      document.getElementsByClassName("btn-registration")[0];
    this.btnRegistration.addEventListener(
      "click",
      this.submitRegistration.bind(this)
    );
  }

  submitLogin(event) {
    event.preventDefault();
    const user=this.loginView.getFormEmailPassword();
    this.loginModel
      .getUser(user)
      .then((res) => {
          localStorage.setItem("user", JSON.stringify({email:user.email, jwt_token:res.jwt_token}));
                this.btnLogin.setAttribute("data-route", "main-page");
      this.btnLogin.click();
      }).catch(err=>{
          console.log(err)
          alert(err);
      });



  }

  submitForgotPassword(event) {
    event.preventDefault();
    const user=this.loginView.forgotPassword();
    
    this.loginModel
      .forgotPasswordUser(user)
      .then((res) => {
        alert(res.message);
          console.log(res);
      }).catch(err=>{
        alert(err);
          console.log(err)
      });


  }

  submitRegistration(event) {
    event.preventDefault();
    const user=this.loginView.registrationUser();

    this.loginModel
      .registrUser(user)
      .then((res) => {
          localStorage.setItem("user", JSON.stringify({email:user.email}));
      this.btnRegistration.setAttribute("data-route", "main-page");
      this.btnRegistration.click();
      }).catch(err=>{
          console.log(err)
      });


  }

}

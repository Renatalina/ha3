import mainPage from "./main-pages.hbs";
import navbar from "./navbar.hbs";

export class MainView{
    constructor(){
        this.init();
    }

    init(){
       const user=JSON.parse(localStorage.getItem("user"));
       this.elementAside=document.getElementsByTagName("aside")[0];
       this.elementAside.innerHTML=mainPage();
       this.elementSection=document.getElementsByTagName("section")[0];
       this.elementSection.innerHTML="";
       this.elementNavBar=document.getElementsByTagName("nav")[0];
       this.elementNavBar.innerHTML=navbar({userName:user.email});
    }

    clearMainForLogout(){
        this.elementAside.innerHTML="";
        this.elementSection.innerHTML="";
        this.elementNavBar.innerHTML="";
    }

}
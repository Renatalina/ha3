import { MainModel } from "./main-model";
import { MainView } from "./main-view";

export class MainPages {
  constructor() {
    this.mainModel = new MainModel();
    this.mainView = new MainView();
  }

  init() {
    this.btnLogOut=document.getElementsByClassName("logout")[0];
    this.btnLogOut.addEventListener("click", this.logoutUser.bind(this));
  }

  logoutUser(){
    this.mainView.clearMainForLogout();
    localStorage.setItem("user", '');
    this.btnLogOut.setAttribute("data-route", "login");
    this.btnLogOut.click();
  }
}

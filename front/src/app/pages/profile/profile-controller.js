import { ProfileModel } from "./profile-model";
import { ProfileView } from "./profile-view";

export class ProfileController {
  constructor() {
    this.profileModel = new ProfileModel();
    this.profileView = new ProfileView();
  }

  init() {
    this.profileModel
      .getUserProfile()
      .then((res) => {
        this.profileView.init(res.user);
      })
      .catch((err) => {
        this.profileView.init();
        console.log(err);
      });
      
      this.initBtn();
  }

  initBtn(){
    const btnEditProfile=document.getElementsByClassName("edit-profile")[0];
    btnEditProfile.addEventListener("click", this. editProfile.bind(this));

    this.btnDelitProfile=document.getElementsByClassName("delit-profile")[0];
    this.btnDelitProfile.addEventListener("click", this.deliteProfile.bind(this));
  }

  editProfile(event){

    event.preventDefauld();
    console.log(event.target);

    this.profileView.editPassword();
    this.btnSaveProfile=document.getElementsByClassName("save-profile")[0];
    this.btnSaveProfile.addEventListener("click", this.saveProfile.bind(this));
  }

  saveProfile(event){

   event.preventDefauld();

   this.profileModel.editProfilePassword(
     this.profileView.getOldAndNewPassword()
   ).then(res=>{
    this.btnSaveProfile.setAttribute("data-route", "profile");
    this.btnSaveProfile.click();
   }).catch(error=>{
     alert(error);
     console.log(error);
   })

  }

  deliteProfile(event){

    event.preventDefauld();

    this.profileModel.deliteProfilePassword()

    .then(res=>{

      console.log(res);
       localStorage.setItem("user", "");

      this.btnDelitProfile.setAttribute("data-route", "login");     
      this.btnDelitProfile.click();
    })
    .catch(error=>{
      alert(error);
    })

  }



}

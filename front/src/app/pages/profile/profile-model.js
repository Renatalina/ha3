export class ProfileModel{
    constructor(){
        this.init()
    }
    init(){
        console.log("profile model");
    }

    async getUserProfile(){
        const responseUser=await fetch("http://localhost:8080/api/users/me",{
            method: "GET",
            mode: "cors",
            headers: {
              "Accept": "application/json",
              "Content-Type": "application/json",
            }
        });
        if (responseUser.ok === true) {
            let user = await responseUser.json();
            return user;
          }
          
          return responseUser.statusText;
    }

    async editProfilePassword(user) {
        const responseUser= await fetch("http://localhost:8080/api/users/me/password", {
            method: "PATCH",
            mode: "cors",
            headers: {
              "Accept": "application/json",
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              oldPassword: user.oldPassword,
              newPassword: user.newPassword
            }),
          })
      
          if (responseUser.ok === true) {
            let user = await responseUser.json();
            return user;
          }
          
          return responseUser.statusText;
        }


        async deliteProfilePassword() {
            const responseUser= await fetch("http://localhost:8080/api/users/me", {
                method: "DELETE",
                mode: "cors",
                headers: {
                  "Accept": "application/json",
                  "Content-Type": "application/json",
                }
              })
          
              if (responseUser.ok === true) {
                let user = await responseUser.json();
                return user;
              }
              
              return responseUser.statusText;
            }
}
import profile from "./profile.hbs";

export class ProfileView{
    constructor(){
        
    }

    init(user){
      this.mainSection=document.getElementsByTagName("section")[0];
      this.mainSection.innerHTML="";
      this.mainSection.innerHTML=profile({userEmail:user.email, userRole: user.role});
    }

    editPassword(){
      const showBlockDivEditPassword=document.getElementsByClassName("edit-password")[0];
      showBlockDivEditPassword.classList.remove("hidden");

      const showBlockBtnEditProfile=document.getElementsByClassName("edit-profile-btn")[0];
      showBlockBtnEditProfile.classList.remove("hidden");

      const hiddenBlockBtnProfile=document.getElementsByClassName("edit-btn")[0];
      hiddenBlockBtnProfile.classList.add("hidden");

      const hiddenBlockEmail=document.getElementsByClassName("unable-email")[0];
      hiddenBlockEmail.classList.add("hidden");

      const hiddenBloclRole=document.getElementsByClassName("unable-role")[0];
      hiddenBloclRole.classList.add("hidden");
    }

    getOldAndNewPassword(){
      
      const form = document.forms["userProfile"];  
      const oldPassword = form.elements["inputPasswordOld"].value;
      const newPassword = form.elements["inputPasswordNew"].value;

      return {oldPassword, newPassword};
    }
}
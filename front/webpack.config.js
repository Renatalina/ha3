const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {
  mode: "none",
  entry: "./src/app/app.js",

  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "bundle.js",
  },
  resolve: {
    modules: [path.resolve(__dirname, "src"), "node_modules"],
    alias: {
      "@less-helpers-module": path.resolve(
        __dirname,
        "src/assets/less/helpers"
      ), // alias for less helpers
      "@assets-root-path": path.resolve(__dirname, "src/assets"), // alias for assets (use for images & fonts)
    },
  },
  module: {
    rules: [
      {
        test: /\.less$/i,
        use: [
          MiniCssExtractPlugin.loader,
          "css-loader",
          "less-loader",
        ],
      },
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"],
      },
      {
        test: /\.(png|jpg|gif)$/i,
        use: [
          {
            loader: "url-loader",
            options: {
              limit: 10000,
            },
          },
        ],
      },
      {
        test: /\.(png|jpe?g|gif)$/i,
        use: [
          {
            loader: "file-loader",
          },
        ],
      },
      {
        test: /\.hbs$/,

        loader: "handlebars-loader",
        options: {
          helperDirs: path.resolve(__dirname, "src/app/hbs_helpers"),
          partialDirs: path.join(__dirname, "src/app/hbs_partials"),
        },
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: ["file-loader"],
      },
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "./style.css",
    }),
    new CopyWebpackPlugin({
      patterns: [
        "src/index.html", // will copy to root of outDir (./dist folder)
        {
          from: "src/static/",
          to: "static",
        },
      ],
    }),
  ],
  devtool: "source-map",
  devServer: {
    // contentBase: "./dist",
    port: 3000,
    historyApiFallback: {
      index: "index.html",
    },
  },
};
